var express = require('express');
var router = express.Router();
const newsDetailsController = require('./../Controllers/newsDetailsController');
/* GET home page. */
router.get('/', (req, res) => {res.send(newsDetailsController.getnewsDetails(req, res));});

module.exports = router;