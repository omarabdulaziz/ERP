var express = require('express');
var router = express.Router();
const newsController = require('./../Controllers/NewsController');
/* GET home page. */
router.get('/', (req, res) => {res.send(newsController.getNews(req, res));});

module.exports = router;