// 20140067
import React, { Component } from 'react';
import './Quote.css';
import img from './../Footer/img4.png';
import mussaq from './mosquee.png';

import shadow2 from './mus-with-shadow2.png';




class Quote extends Component {
  render() {

     
    return (
        
        <div id="mosque-container"  >
            <img id="mosque1" src={mussaq} alt="mussaq"/>
            <div className="container">
                <div className="row">
                    <div id="mus-on-mosque-cont" className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    <img id="mus-on-mosque" src= {shadow2} alt="mustashar"/>
                    </div>
                    <div id="saying-container" className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <p>“ إن المسلمين صنعوا حضارة راقية قامت على العلم والمعرفة والتجربة، وسعد بها الناس شرقًا وغربًا، تحت ظلال هذا الدين الحنيف .“</p>
                    <p>المستشار محمد عبدالسلام</p>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default Quote;