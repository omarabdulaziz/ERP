import React, { Component } from 'react';
// 20140126
import Header from '../../Header/Header';
import BioPage from '../BioPage';
import Footer from '../../Footer/Footer';
import Quote from '../../Quote/Quote';




class BioComponent extends Component {
  render() {
    return (
      <div className="App">
      <Header/>
      <BioPage/>
      <Quote/>
      <Footer/>
      </div>

    );
  }
}

export default BioComponent;
