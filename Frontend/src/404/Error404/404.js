// 20140067
import React, { Component } from 'react';



import './404.css';

class ERROR extends Component {
  render() {
      
    return (
        <div className="container mainPart404" >
        <div className="row">
            <div className="col-md-12 col-sm-12  Message404">
              <p> 404</p>
            </div>
        
         </div>
         <div className="row text1">
             <div className="col-md-12 col-sm-12 ">
                 <h2 className="notfound">الصفحة غير موجودة</h2>
                  <p className="notfoundMessage">عذرا ، ولكن الصفحة التي كنت تبحث عنها لم يتم العثور عليها
                      <br/>
                        حاول التحقق من الرابط للخطأ,ثم اضغط
                      على زرالتحديث <br/>في المتصفح الخاص بك
                      
                          اوحاول العثورعلى شي اخر في موقعنا</p>
             </div>
        
         </div>
         <div className="row" >
            <div className="col-md-12 col-sm-12"  >
             <a href="#" className="back"> <h4 >الرجوع إلي الصفحة الرئيسية</h4> </a>
             </div>
        
         </div>
        
        
        </div>
    );
  }
}

export default ERROR;