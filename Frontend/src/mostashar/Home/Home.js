
import React, { Component } from 'react';

import './home.css';
import img from './../../Footer/img4.png';
import mussaq from './../../Quote/mosquee.png';

import shadow2 from './homepic.png';

class Home extends Component {
  render() {

     
    return (
        
        <div id="mosque-container-home"  >
            <img id="mosque1" src={mussaq} alt="mussaq"/>
            <div className="container">
                <div className="row">
                    <div id="mus-on-mosque-cont" className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    <img id="mus-on-mosque" src= {shadow2} alt="mustashar"/>
                    </div>
                    <div id="saying-container" className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <img src={img} alt="logo" id="home-logo" className="col-5" style={{textAlign:"center"}}/>
                    <h5 className="col-10" id="homequote"  >المستشار القانوني لفضيلة الإمام الأكبر شيخ الأزهر الشريف</h5>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default Home;