// 20140067
import React, { Component } from 'react';
import './News.css';
import NewsBIGCard from '../NewsBIGCard/NewsBIGCard';
import NewsText from '../NewsText/NewsText'
import SmallCard from '../SmallCard/SmallCard';
// import flogo from './mostashar/BioCard/facebook.png'
// import tlogo from './mostashar/BioCard/twitter.png'

class News extends Component {
    
  render() {
    return (

    
         <div className="grayBackground">
            <div className="container marginTop">
                    
                    <div className="row" >
                    < NewsText/>
                </div>
                </div>
                <div className="container bioInfo">
                < div className="row"  >
                        <div className="col-xs-6 col-md-6 col-lg-6" >   <NewsBIGCard/></div>
                        <div className="col-xs-6 col-md-6 col-lg-6" >  
                        < div className="row"  >
                        <div className="col-xs-6 col-md-6 col-lg-6" >  <SmallCard/></div>
                        <div className="col-xs-6 col-md-6 col-lg-6" >  <SmallCard/></div>
                        </div>
                        < div className="row"  >
                        
                        <div className="col-xs-6 col-md-6 col-lg-6" >  <SmallCard/></div>
                        <div className="col-xs-6 col-md-6 col-lg-6" >  <SmallCard/></div>
                        </div>
                        
                        </div>
                           
                           
                </div>
               
                </div>

        </div>
    );
  }
}

export default News;