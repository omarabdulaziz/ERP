// 20140067
import React, { Component } from 'react';
import './BioCard.css';
import flogo from './facebook.png'
import tlogo from './twitter.png'

class BioCard extends Component {
    
  render() {
    const style = 
    {

        width: '18rem',
        textAlign: 'right',
        
    };
    return (

        <div className="card-deck" style= {style}>
        
        <div className="card-body ">
        <div className="border" style={{background: 'white'}} >    
          <h5 className="card-title mainColor">الدرجة العلمية</h5>
          <p className="card-text gray">-عمل معيداً، ومدرساً مساعداً، ومدرساً، وأستاذاً مساعداً للعقيدة والفلسفة بجامعة الأزهر، وحالياً أستاذ للعقيدة والفلسفة في نفس الجامعة.</p>      
      <div className="line"  >
          <a className="mainColor">المزيد </a>
          <img  align="left" src={flogo} alt="lolo" />
          <img  className="logo" align="left" src={tlogo}alt="lolo"  />
          
      </div>
            </div>
        </div>
      </div>
    );
  }
}

export default BioCard;