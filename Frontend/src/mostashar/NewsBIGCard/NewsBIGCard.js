
// 20140067
import React, { Component } from 'react';
import './NewsBIGCard.css';
import flogo from './../BioCard/facebook.png'
import tlogo from './../BioCard/twitter.png'
import emam from './emam.png'

class NewsBIGCard extends Component {
    
  render() {
      const style={
        width: '575px'
      }
    return (

    
        <div className="card-deck" style={style}>
                    <div className="cardss" >
                          <img className="card-img-top" src={emam} alt="Card "/>
                          <div className="card-body card-bodyM">
                                <h4 className="card-title">قال فضيلة الإمام الأكبر: إن القرآن الكريم استعمل منذ البداية أسلوب الحوار مع الشباب، وليس أسلوب الإملاء والأمر والنهى، وعَرَض لأنموذجين: أحدهما للشاب المتمرد الذي يستعلي ويستكبر ولا يستجيب</h4>
                                <p className="card-text">قال فضيلة الإمام الأكبر: إن القرآن الكريم استعمل منذ البداية أسلوب الحوار مع الشباب، وليس أسلوب الإملاء والأمر والنهى، وعَرَض لأنموذجين: أحدهما للشاب المتمرد الذي يستعلي ويستكبر ولا يستجيب، والأنموذج الثاني للشاب الواعي</p>
                                 <div className="line"  >
                                    <a className="mainColor">المزيد </a>
                                    <img  align="left" src={flogo} alt="lolo"/>
                                    <img  className="logo" align="left" src={tlogo} alt="lolo" />
                                </div>
                        </div>
                    </div>
                </div>
    );
  }
}

export default NewsBIGCard;