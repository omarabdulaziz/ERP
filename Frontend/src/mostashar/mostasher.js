// 20140067
import React, { Component } from 'react';

import './mostasher.css';


import Bio from './Bio/Bio';
import News from  './News/News';
import Footer from './../Footer/Footer';
import Header from './../Header/Header';
import Quote from './../Quote/Quote';
import Home from './Home/Home';
import MediaMainSlider from './../Media/components/media_main_slider';
class Mostasher extends Component {
  render() {
    return (
      <div className="App">
   
       <Home/>
       <Bio/>
       <News/>
       <MediaMainSlider/>
      <Quote/>
  
       
      </div>
    );
  }
}

export default Mostasher;