import React, { Component } from 'react';
import {Router , Route , browserHistory,IndexRoute} from "react-router";

import Bio from './mostashar/Bio/Bio';
import News from  './mostashar/News/News'
import Footer from './Footer/Footer';
import Header from './Header/Header';
import Quote from './Quote/Quote';
import Error_404 from './404/Error404';

import NewsDetails from './NewsDetails/newsDetails';

import Root from './Root/Root';
import Search from './Search/Search';
import MediaAppComponent from './Media/Media_App';


import Mostashar from './mostashar/mostasher';
import MyNews from './myNews/News/news';
import Contactus from './ContactUs/Contactus/Contactus';
class App extends Component {
  render() {
    return (
      <div className="App">
       <Router history={browserHistory}>
       <Route path={"/"} component={Root}>
            <IndexRoute component={Mostashar} />
            <Route path={"/search"} component={Search} ></Route>
            <Route path={"/newsDetails"} component={NewsDetails} ></Route>
            <Route path={"/bio"} component={Bio} ></Route>
            <Route path={"/news"} component={MyNews} ></Route>
            <Route path={"/contact"} component={Contactus} ></Route>
            <Route path={"/media"} component={MediaAppComponent} ></Route>
       </Route>
       <Route path='*' exact={true} component={Error_404} />
       </Router>
      </div>

    );
  }
}

export default App;
