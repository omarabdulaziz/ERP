import React, { Component } from 'react';
import './newsDetails.css';
import NewsBIGCard from './BigCardNewsdetails/bigCardNewsDea';
import SmallCard from '../mostashar/SmallCard/SmallCard';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';


class NewsDetails extends Component {
 constructor() {
    super();
    this.state = {
      smallCards: [],
      bigCards:[]
    };
  }
  componentDidMount() {
    fetch("http://localhost:4200/news")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            smallCards: result
          });
         
        });

    fetch("http://localhost:4200/newsDetails")
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          bigCards: result
        });
        
      });
    
      
  }
  render() {
    console.log(this.props.params.id);
    const style={
     margin:"400 0 0 5",
    }
    return (
         <div main-Part >
                <Header/>
                <div className="container ">
                < div className="row" >
                        <div className="col-xs-8 col-md-8 col-lg-8 big" > 
                        {this.state.bigCards.map((card,index) => {
                          return <NewsBIGCard
                            title={card.title}
                            image={card.image}
                            date={card.date}
                            d={card.d}
                            key={card.id}
                                 />
                        })}
                         
                          </div>
                        <div className="col-xs-3 col-md-3 col-lg-3 small"  > 
                        {this.state.smallCards.map((card,index) => {
                          return <SmallCard
                            title={card.title}
                            image={card.image}
                            key={card.id}
                                 />
                        })}
                           
                        </div>    
                           
                </div>
                </div>
              

        </div>
    );
  }
}

export default NewsDetails;