

import React, { Component } from 'react';
import './bigCardNewsDea.css';
import flogo from './facebook.png';
import tlogo from './twitter.png';
import emam from './emam.png';

class NewsBIGCard extends Component {
    
  render() {
     
      const style2={
        color:"#cccccc",
        font: "13px"

        
      }
    return (

      <div className="cardHead"  >
      <div className="Mycardss" >
            <img className="card-img-top" src={this.props.image} alt="Card "/>
            <div className="card-body MYcard-body">
            <img  align="left" src={flogo} alt="lolo"/>
                      <img  className="MYlogo" align="left" src={tlogo} alt="lolo" />
                      
                  <h4 className="card-title">{this.props.title}</h4>
                  <p className="date pull-right ">  {this.props.date}</p>
                  <div className="main-card-artical" >
                  <p style={style2}>
                    {this.props.d}
                  </p>
                  </div>
                 
          </div>
      </div>
  </div>


    );
  }
}

export default NewsBIGCard;






