import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap';
import './Media_App.css';
import MediaCardsContainer from './components/media_cards_container';
import MosqueQuote from './components/mosque_quote';
import MediaMainSlider from './components/media_main_slider';
import MediaModalSlider from './components/media_modal_slider';
import $ from 'jquery';


class MediaAppComponent extends Component {


  render() {

    return(
      <div id="med">

        <MediaMainSlider />
        <MediaModalSlider />
        <MediaCardsContainer moreLink="false"/>
        <MosqueQuote />
        <MediaCardsContainer moreLink="true"/>
    </div>
    );
  }

  componentDidMount(){
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that   opens the modal
    var btn = document.getElementById("carouselExampleControls");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    /*btn.dblclick = function() {
        modal.style.display = "block";
    }*/
    $( ".carousel-item" ).click(function() {
    modal.style.display = "block";
    });
    // When the user clicks on <span> (x), close the modal
    /*span.onclick = function() {
        modal.style.display = "none";
    }*/
    $( span  ).click(function() {
    modal.style.display = "none";
    });

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }

      var slideIndex = 1;
      showDivs(slideIndex);

      function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        console.log(x);
        if (n > x.length) {slideIndex = 1}
        if (n < 1) {slideIndex = x.length}
        for (i = 0; i < x.length; i++) {
           x[i].style.display = "none";
        }
        x[slideIndex-1].style.display = "block";
      }

    function plusDivs(n) {
      showDivs(slideIndex += n);
    }

  }

}

export default MediaAppComponent;
