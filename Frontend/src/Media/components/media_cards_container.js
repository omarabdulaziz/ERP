import React from 'react';
import './media_cards_container.css';
import MediaCard from './media_card.js';
import MediaMoreLink from './media_more_link';

const MediaCardsContainer = ({moreLink}) =>{

  var Cards = [];
  var numrows = 8;
  for (var i = 0; i < numrows; i++) {
      Cards.push(<MediaCard key={i} />);
  }

  var mediaMoreLink = "";
  if(moreLink==="true"){
    mediaMoreLink = <MediaMoreLink />;
  }
  
  return(
    <div id="media-body-fluid-container" className="container-fluid">
      <div className="container">
       <div className="row">
        {Cards}
       </div>
       {mediaMoreLink}
     </div>
   </div>
  );
};

export default MediaCardsContainer;
