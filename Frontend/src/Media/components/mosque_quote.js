import React from 'react';
import './mosque_quote.css';

const MosqueQuote = () => {

  return (
    <div id="mosque-container11" className="container-fluid" >
        <img id="mosquee" src={require("../images/mussaq.jpg")} alt="mussaq" />
        <div id="mus-on-mosque-cont11" className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <img id="mus-on-mosque11" src={require("../images/mus-with-shadow2.png")} alt="mustashar" />
        </div>
        <div id="saying-container11" className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <p>“ إن المسلمين صنعوا حضارة راقية قامت على العلم والمعرفة والتجربة، وسعد بها الناس شرقًا وغربًا، تحت ظلال هذا الدين الحنيف .“</p>
          <p>المستشار محمد عبدالسلام</p>
        </div>
    </div>
  );
}

export default MosqueQuote;
