import React from 'react';
import './media_card.css';


const MediaCard = () => {
  //var imgSrc = '../images/mus.jpg';
  return (
    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <img id="media_card_img" src={require('../images/mus cropped.jpg')} alt="mustashar pic" />
        <p className="img-description-media"> كتاب يحكي عن الجانب النقدي في فلسفة أبي البركات البغدادي.</p>
    </div>
  );
}

export default MediaCard;
