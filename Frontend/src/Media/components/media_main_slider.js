import React from 'react';
import './media_main_slider.css';



const MediaMainSlider = () =>{



  return(
  <div>
    <div id="media-slider-container" className="container-fluid">
      <div id="media-slider" className="container">

        <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img className="d-block w-100" src={require('../images/293932_0.jpg')}  alt="First slide"/>
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={require('../images/slide2.jpg')} alt="Second slide"/>
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={require('../images/mus cropped.jpg')} alt="Third slide"/>
          </div>
        </div>

        <div className="slider-cursors center">
        <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="sr-only">Next</span>
        </a>
      </div>

      </div>

      </div>
    </div>


  </div>
  );
}


export default MediaMainSlider;
