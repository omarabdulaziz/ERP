import React,{Component} from 'react';
import './media_modal_slider.css';


class MediaModalSlider extends Component{

  constructor(props){
    super(props);
     this.state = {
       slideIndex:1
      };
  }




  render(){
  return(
    <div id="myModal" className="modal">

      <div className="modal-content">
          <span className="close">&times;</span>
        <div className="row">
        <div id="modal-slider-container" className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div className="w3-content w3-display-container">
          <div className="w3-display-container mySlides ">
            <img className="slides-img" src={require('../images/293932_0.jpg')} />
          </div>

          <div className="w3-display-container mySlides">
            <img className="slides-img" src={require('../images/mus cropped.jpg')} />
          </div>

          <div className="w3-display-container mySlides">
            <img className="slides-img" src={require('../images/slide2.jpg')} />
          </div>

          <button className="w3-button w3-display-right w3-black" onClick={ ()=>{
               var temp = this.state.slideIndex - 1;
               this.state.slideIndex = temp;
               var n = this.state.slideIndex;
                var i;
                var x = document.getElementsByClassName("mySlides");
                if (n > x.length) {this.state.slideIndex = 1; }
                if (n < 1) {this.state.slideIndex = x.length ;  }
                for (i = 0; i < x.length; i++) {
                   x[i].style.display = "none";
              }
              x[this.state.slideIndex-1].style.display = "block";
            }}>&#10092;</button>
          <button className="w3-button w3-display-left w3-black" onClick={ ()=>{
               var temp = this.state.slideIndex + 1;
               this.state.slideIndex = temp;
               var n = this.state.slideIndex;
                var i;
                var x = document.getElementsByClassName("mySlides");
                if (n > x.length) {this.state.slideIndex = 1; }
                if (n < 1) {this.state.slideIndex = x.length ;  }
                for (i = 0; i < x.length; i++) {
                   x[i].style.display = "none";
              }
              x[this.state.slideIndex-1].style.display = "block";
            }}>&#10093;</button>
        </div>
      </div>
      <div id="modal-info-container" className="col-lg-6 col-md-6 col-sm-12 col-xs-12">

        <div id="Modal-slider-description">
          <p className=""> كتاب يحكي عن الجانب النقدي في فلسفة أبي البركات البغدادي.</p>
        </div>
        <div id="modal-icons" className="">
          <span id="modal-slider-counter">
            1/8
          </span>
            <span className="modal-fa-icons"><a href="#"><i className="fa fa-google-plus"></i></a></span>
            <span className="modal-fa-icons"><a href="#"><i className="fa fa-linkedin"></i></a></span>
            <span className="modal-fa-icons"><a href="#"><i className="fa fa-instagram"></i></a></span>
            <span className="modal-fa-icons"><a href="#"><i className="fa fa-twitter"></i></a></span>
            <span className="modal-fa-icons"><a href="#"><i className="fa fa-facebook-square"></i></a></span>
        </div>
      </div>

        </div>

      </div>


    </div>
  );
}


}


export default MediaModalSlider;
