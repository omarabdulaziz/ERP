import React, { Component } from 'react';

import GoogleMapReact from 'google-map-react';
import '.././ContactusDetails/ContactusDetails.css'

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class ContactusMap extends Component
{

    static defaultProps = {
        center: {
          lat: 38.885516,
          lng: -77.09327200000001
        },
        zoom: 11
      };
    render() {
        // $(function ($) {
        //     function init_map1() {
        //         var myLocation = new google.maps.LatLng(38.885516, -77.09327200000001);
        //         var mapOptions = {
        //             center: myLocation,
        //             zoom: 16
        //         };
        //         var marker = new google.maps.Marker({
        //             position: myLocation,
        //             title: "موقعنا"
        //         });
        //         var map = new google.maps.Map(document.getElementById("map1"),
        //             mapOptions);
        //         marker.setMap(map);
        //     }
        //     init_map1();
        // });
        return (
            <div class="container text-right">
                <h3 class="ourPlace" >موقعنا</h3>
                <br/>
                <div class="row">
                <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyDqKN4L390Os5t8P_VvvKgKo5XrsQL5gso&libraries=places&callback=initAutocomplete' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={59.955413}
            lng={30.337844}
            text={'Kreyser Avrora'}
          />
        </GoogleMapReact>
      </div>

                </div>
            </div>
        );
    }
}

export default ContactusMap;