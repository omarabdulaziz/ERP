import React, { Component } from 'react';
import ContactusDetails from './../ContactusDetails/ContactusDetails';
import ContactusMap from './../ContactusMap/ContactusMap';
import Quote from './../../Quote/Quote';
class Contactus extends Component
{
    render() {
        return (
            <div>
                <ContactusDetails/>
                <ContactusMap/>
                <Quote/>
            </div>
        );
    }
}

export default Contactus;