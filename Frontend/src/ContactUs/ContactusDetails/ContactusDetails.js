import React, { Component } from 'react';
import './ContactusDetails.css'

class ContactusDetails extends Component
{
    render() {
        const callUsstyle={
            marginTop : "60px",
            direction:"ltr"
        }
        
        return (

            <div class="container ">
    <div class="row callus">
        <div class="col-sm-12">
        <p class="callus-Text" style={callUsstyle}>أتصل بنا </p> <hr/>
        </div>

    </div>
    <div class="row contact-part" />
    <div class="col-sm-1"></div>
  <div style={{direction:"ltr"}}>
    <div class="col-sm-4 form-part" style={{display: "inline-block",verticalAlign: "top"}}>
    <h5 class="topHeaders">تواصل معنا</h5>

    <form>
    <div class="form-group">
        <input type="text" class="form-control" id="Name" placeholder="الاسم"/>
    </div>
    <div class="form-group">
        <input type="email" class="form-control" id="Email" aria-describedby="emailHelp" placeholder=" البريد الالكتروني"/>
    </div>

    <div class="form-group">
        <textarea class="form-control" id="meassage" rows="5" placeholder="الرسالة"></textarea>
    </div>
    <button type="submit" class="send-btn btn pull-right send" >أرسال</button>
    </form>
    </div>


    <div class="col-sm-4 call-info" style={{display: "inline-block"}}>
            <h4 class="topHeaders"  >بيانات الاتصال </h4>
            <div>
                <p class="contact_details_herder">:البريد الالكتروني</p>
                <p class="info">el_azhar@mail.com</p>
                <p class="contact_details_herder">:العنوان</p>
                <p class="info"> الدور السادس - مبنى مشيخه الازهر - بجوار الحديقه - القاهرة</p>
                <p class="contact_details_herder">:تليفون</p>
                <p class="info">29222222 - 29333333</p>
                <p class="contact_details_herder">:الموقع</p>
                <p class="info"> www.alemamalakbar.eg</p>

            </div>

    </div>
    </div>
    </div>

        );
    }
}

export default ContactusDetails;