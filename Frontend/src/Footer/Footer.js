// 20140067


import React, { Component } from 'react';
import './Footer.css';
import img from './img4.png';




class Footer extends Component {
  render() {
      const style={
        clear:'both',
        // padding:'40px 0px 40px',
        backgroundColor:'#232323'

      }

      const style2={
        //paddingRight:'35px'

      }
      const style3={
        paddingLeft: '150px',
         margin: '0'

      }
      
      const style4={
        paddingLeft:'0px'

      }

      const style5={
        width:'150px', 
        height:'170px'

      }
      
      
      

    return (
      <div className="App">
      

    <div  className="AppFooter" style={style}>
 	<div class="container" style={style2}>
			<div class="row direc" >

			  <div className="card footer col-xs-2 col-md-2 col-lg-2">  
			      <div className="card-body  padd">
			       <h4>السيرة الذاتية</h4>
			       <p>مولده وتعليمه</p>
			       <p>الدرجة العلمية</p>
			       <p>حياته قبل الأزهر</p>
			      </div>  
			  </div>

			  <div className="card footer col-xs-2 col-md-2 col-lg-2">  
			      <div className="card-body">
			       <h4>أخبار</h4>
			       <p>آخر الأخبار</p>
			       <p>مقابلات مع الامام</p>
			       <p>مؤتمرات الأزهر</p>
			      </div>  
			  </div>

			  <div className="card footer col-xs-2 col-md-2 col-lg-2">  
			      <div className="card-body">
			       <h4>وسائط</h4>
			       <p>صور</p>
			       <p>فيديوهات</p>
			      </div>  
			  </div>

			  <div className="card footer col-xs-3 col-md-3 col-lg-3" style={style3}>  
			      <div className="card-body">
			       <h5 style={{fontSize: "1rem",marginBottom: "20px"}}>تواصل معنا</h5>
				   <input id="example1" type="text" placeholder="اشترك معنا"/>
			      </div>  
			  </div>

			  <div className="card footer col-xs-2 col-md-2 col-lg-2" style={style4}>
			      <img className="card-img-top" src={img} alt="Card " style={style5}/>   
			  </div>
			</div>
		</div>
</div>
      </div>

    );
  }
}

export default Footer;