
import React, { Component } from 'react';
import Header from './../Header/Header';
import Footer from './../Footer/Footer';

class Root extends Component {
  render() {

     
    return (
        
        <div id="mosque-container"  >
           <Header/>
           {this.props.children}
           <Footer/>
        </div>
    );
  }
}

export default Root;