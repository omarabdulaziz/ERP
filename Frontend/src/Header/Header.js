// 20140067
import React, { Component } from 'react';
import './Header.css';
import img from './../Footer/img4.png';
import {Link} from "react-router";



class Header extends Component {
  render() {

    const style={
        marginLeft:'157px'

      }

      const style2={
        color: 'white',
        fontSize:'16p'

      }
      const style3={
        width:'90px',
        height: '70px'

      }
      const style4={
        color:'#ac9a6a'

      }
     
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light myHeader">
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
              <li><a href="#"><i className="fa fa-facebook-square"></i></a></li>
              <li><a href="#"><i className="fa fa-twitter" style={style4}></i></a></li>
              <li><a href="#"><i className="fa fa-instagram"></i></a></li>
              <li><a href="#"><i className="fa fa-google-plus"></i></a></li>
              <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
              <li><a href="#"><i className="fa fa-search"></i></a></li>
          </ul>

          <ul className="nav navbar-nav navbar-left">             
              <li><Link to={"/contact"} style={style} activeClassName={"myA"} activeStyle={style2} >اتصل بنا</Link></li>
              <li><Link to={"/media"} activeClassName={"myA"} activeStyle={style2} >ميديا</Link></li>
              <li><Link to={"/news"} activeClassName={"myA"} activeStyle={style2} >أخبار</Link></li>
              <li><Link to={"/bio"} activeClassName={"myA"} activeStyle={style2} >السيرة الذاتية</Link></li>      
          </ul>
          <a className="navbar-brand" href="#">
              <img src={img} alt="Logo" style={style3}/>
            </a>
        </div>
</nav>

    );
  }
}

export default Header;