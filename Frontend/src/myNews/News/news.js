import React, { Component } from 'react';

import BigCard from '.././BigCard/bigCard';
import MysmallCard from '.././SmallCard/smallCard';
import Quote from './../../Quote/Quote';

class MyNews extends Component
{
    render() {
        return (
            <div>
                <BigCard/>
                <MysmallCard/>
                <Quote/>
                <MysmallCard/>

            </div>
        );
    }
}

export default MyNews;