import React, { Component } from 'react';
import './bigCard.css';
import Card from './Card1';
import Card2 from './Card2';

class BigCard extends Component
{
    render() {

        return (
            <div class="container-fluid">
            <div class="row">
                <Card/>
                <Card2/>
                <Card/>
            </div> 
         </div>
        );
      }
}

export default BigCard;