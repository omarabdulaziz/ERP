import React, { Component } from 'react';
import './bigCard.css';
import img from './img1.png' 

class Card extends Component
{
    render() {
        const mystyle={
            width: '100%',
            height:"621px",
            backgroundImage: "url(" + img + ")"
          }

        return (
                 <div class="card newsHeader1 col-lg-4" style={mystyle}>
                     <div class = "newsContent">
                         <h4>قال فضيلة الإمام الأكبر: إن القرآن الكريم استعمل منذ البداية أسلوب الحوار مع الشباب , وليس اسلوب الإملاء</h4>
                         <p>5 يوليو 2016</p>
                     </div>
                </div>
        );
      }
}

export default Card;